package org.caes.exercici1;
import static org.junit.Assert.*;
import static junitparams.JUnitParamsRunner.*;

import java.util.*;

import org.junit.*;
import org.junit.runner.*;

import junitparams.*;

@RunWith(JUnitParamsRunner.class)
public class TestAdd
{
    @Test
    @Parameters({
            "1, 1, 0, '1+0 should be 1'",
            "1, 0, 1, '0+1 should be 1'" ,
            "0, 0, 0, '0+0 should be 0'"})
    public void testSumZero(double expected, double v1, double v2, String explain) {
        Calculator calc = new Calculator();
        assertEquals(explain, expected, calc.add(v1, v2), 0);
    }

    @Test
    @Parameters({
            "0,-1, 1, '-1+1 should be 0'" ,
            "0, 1,-1, '1+(-1) should be 0'",
            "-2, -1, -1, '-1+(-1) should be -2'"})
    public void testSumNegative(double expected, double v1, double v2, String explain) {
        Calculator calc = new Calculator();
        assertEquals(explain, expected, calc.add(v1, v2), 0);
    }

    @Test
    @Parameters({
            "2, 1, 1, '1+1 should be 2'" , // expected, v1, v2, explain
            "16, 8, 8, '8+8 should be 16'"})
    public void testSumPositive(double expected, double v1, double v2, String explain) {
        Calculator calc = new Calculator();
        assertEquals(explain, expected, calc.add(v1, v2), 0);
    }

    @Test
    @Parameters({
            "Infinity, Infinity, 1, 'Infinity+1 should be Infinity'" , // expected, v1, v2, explain
            "NaN, Infinity, -Infinity, 'Infinity+(-Infinity) should be NaN'" ,
            "Infinity, Infinity, Infinity, 'Infinity+Infinity should be Infinity'"})
    public void testSumInfinity(double expected, double v1, double v2, String explain) {
        Calculator calc = new Calculator();
        assertEquals(explain, expected, calc.add(v1, v2), 0);
    }

    @Test
    @Parameters({
            "NaN, NaN, 1, 'NaN+1 should be NaN'" , // expected, v1, v2, explain
            "NaN, Infinity, NaN, 'Infinity+NaN should be NaN'" ,
            "NaN, NaN, NaN, 'NaN+NaN should be NaN'"})
    public void testSumNaN(double expected, double v1, double v2, String explain) {
        Calculator calc = new Calculator();
        assertEquals(explain, expected, calc.add(v1, v2), 0);
    }
}


