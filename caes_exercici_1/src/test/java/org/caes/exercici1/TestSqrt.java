package org.caes.exercici1;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class TestSqrt
{
    @Test
    @Parameters({"0, 0, 'Sqrt(0) should be 0'"})
    public void testSqrtZero(double expected, double v1, String explain) {
        Calculator calc = new Calculator();
        assertEquals(explain, expected, calc.sqrt(v1), 0);
    }

    @Test
    @Parameters({
            "2, 4, 'Sqrt(4) should be 2'" , // expected, v1,explain
            "1, 1, 'Sqrt(1) should be 1'"})
    public void testSqrtPositive(double expected, double v1, String explain) {
        Calculator calc = new Calculator();
        assertEquals(explain, expected, calc.sqrt(v1), 0);
    }

    @Test
    @Parameters({
            "NaN, -Infinity, 'Sqrt(-Infinity) should be NaN'" ,
            "Infinity,Infinity, 'Sqrt(Infinity) should be Infinity'" })
    public void testSqrtInfinity(double expected, double v1, String explain) {
        Calculator calc = new Calculator();
        assertEquals(explain, expected, calc.sqrt(v1), 0);
    }

    @Test
    @Parameters({
            "NaN, -4, 'Sqrt(-4) should be NaN'"})
    public void testSqrtNegative(double expected, double v1, String explain) {
        Calculator calc = new Calculator();
        assertEquals(explain, expected, calc.sqrt(v1), 0);
    }

    @Test
    @Parameters({"NaN, NaN, 'Sqrt(NaN) should be NaN'"})
    public void testSqrtNaN(double expected, double v1, String explain) {
        Calculator calc = new Calculator();
        assertEquals(explain, expected, calc.sqrt(v1), 0);
    }

}
