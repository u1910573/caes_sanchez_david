package org.caes.exercici1;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class TestMultiply
{
    @Test
    @Parameters({
            "4, 2, 2, '2*2 should be 4'",
            "1, 1, 1, '1*1 should be 1'"})
    public void testMultPositive(double expected, double v1, double v2, String explain) {
        Calculator calc = new Calculator();
        assertEquals(explain, expected, calc.multiply(v1, v2), 0);
    }

    @Test
    @Parameters({
            "1,-1, -1, '-1*(-1) should be 1'" ,
            "-1, 1, -1, '1*(-1) should be -1'"})
    public void testMultNegative(double expected, double v1, double v2, String explain) {
        Calculator calc = new Calculator();
        assertEquals(explain, expected, calc.multiply(v1, v2), 0);
    }

    @Test
    @Parameters({
            "0, 0, 0, '0*0 should be 0'",
            "0, 1, 0, '1*0 should be 0'"})
    public void testMultZero(double expected, double v1, double v2, String explain) {
        Calculator calc = new Calculator();
        assertEquals(explain, expected, calc.multiply(v1, v2), 0);
    }

    @Test
    @Parameters({
            "NaN, -Infinity, 0, '-Infinity*0 should be NaN'",
            "NaN, Infinity, 0, 'Infinity*0 should be 0'" ,
            "-Infinity,Infinity, -Infinity, 'Infinity*(-Infinity) should be -Infinity'" })
    public void testMultInfinity(double expected, double v1, double v2, String explain) {
        Calculator calc = new Calculator();
        assertEquals(explain, expected, calc.multiply(v1, v2), 0);
    }

    @Test
    @Parameters({
            "NaN, NaN, 1, 'NaN*1 should be NaN'",
            "NaN, NaN, Infinity, 'NaN*Infinity should be NaN'", })
    public void testMultNaN(double expected, double v1, double v2, String explain) {
        Calculator calc = new Calculator();
        assertEquals(explain, expected, calc.multiply(v1, v2), 0);
    }

    @Test (expected = Exception.class)
    @Parameters({
            "500, 2", //v1, v2
            "Infinity, Infinity",
            "-Infinity, -Infinity",
            "1, Infinity" })
    public void testMultException(double v1, double v2) {
        Calculator calc = new Calculator();
        calc.multiply(v1, v2);
    }
}
