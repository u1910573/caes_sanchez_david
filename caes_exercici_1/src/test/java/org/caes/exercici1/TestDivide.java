package org.caes.exercici1;
import static org.junit.Assert.*;
import static junitparams.JUnitParamsRunner.*;

import java.util.*;

import org.junit.*;
import org.junit.runner.*;

import junitparams.*;

@RunWith(JUnitParamsRunner.class)
public class TestDivide
{
    @Test
    @Parameters({
            "1, 1, 1, '1/1 should be 1'" , // expected, v1, v2, explain
            "2, 4, 2, '4/2 should be 2'"})
    public void testDivPositive(double expected, double v1, double v2, String explain) {
        Calculator calc = new Calculator();
        assertEquals(explain, expected, calc.divide(v1, v2), 0);
    }

    @Test
    @Parameters({
            "1, -1, -1, '-1/(-1) should be 1'",
            "-1, -1, 1, '(-1)/1 should be -1'" })
    public void testDivNegative(double expected, double v1, double v2, String explain) {
        Calculator calc = new Calculator();
        assertEquals(explain, expected, calc.divide(v1, v2), 0);
    }

    @Test
    @Parameters({
            "Infinity, 1, 0, '1/0 should be infinity'" ,
            "0, 0, 1, '0/1 should be 0'",
            "0, 0, -1, '0/(-1) should be 0'",
            "-Infinity, -1, 0, '-1/0 should be -infinity'",
            "NaN, 0, 0, '0/0 should be NaN'"})
    public void testDivZero(double expected, double v1, double v2, String explain) {
        Calculator calc = new Calculator();
        assertEquals(explain, expected, calc.divide(v1, v2), 0);
    }

    @Test
    @Parameters({
            "Infinity, Infinity, 1, 'Infinity/1 should be Infinity'",
            "0, 1, Infinity, 1/Infinity should be 0'"})
    public void testDivInfinity(double expected, double v1, double v2, String explain) {
        Calculator calc = new Calculator();
        assertEquals(explain, expected, calc.divide(v1, v2), 0);
    }

    @Test
    @Parameters({
            "NaN, NaN, 1, 'NaN/1 should be NaN'",
            "NaN, NaN, -1, 'NaN/-1 should be NaN'",
            "NaN, NaN, 0, 'NaN/0 should be NaN'",
            "NaN, NaN, Infinity, NaN/Infinity should be NaN'"})
    public void testDivNaN(double expected, double v1, double v2, String explain) {
        Calculator calc = new Calculator();
        assertEquals(explain, expected, calc.divide(v1, v2), 0);
    }
}
