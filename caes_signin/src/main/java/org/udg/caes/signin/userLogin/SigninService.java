/*
 * Copyright (c) 2006-2011 Rogério Liesenfeld
 * This file is subject to the terms of the MIT license (see LICENSE.txt).
 */
package org.udg.caes.signin.userLogin;

import org.udg.caes.signin.userAccount.*;

public final class SigninService {
	private static final int MAX_LOGIN_ATTEMPTS = 3;

	private int loginAttemptsRemaining = MAX_LOGIN_ATTEMPTS;
	private String previousAccountId;
	private UserAccount account;

	public void signin(String accountId, String password)
			throws UserAccountNotFoundException, UserAccountRevokedException,
			AccountLoginLimitReachedException {
		account = UserAccount.find(accountId);

		if (account == null) {
			throw new UserAccountNotFoundException();
		}

		if (account.passwordMatches(password)) {
			registerNewLogin();
		} else {
			handleFailedLoginAttempt(accountId);
		}
	}

	private void registerNewLogin() throws AccountLoginLimitReachedException,
			UserAccountRevokedException {
		if (account.isSignedIn()) {
			throw new AccountLoginLimitReachedException();
		}

		if (account.isRevoked()) {
			throw new UserAccountRevokedException();
		}

		account.setSignedIn(true);
		loginAttemptsRemaining = MAX_LOGIN_ATTEMPTS;
	}

	private void handleFailedLoginAttempt(String accountId) {
		if (previousAccountId == null || accountId.equals(previousAccountId)) {
			loginAttemptsRemaining--;
		} else {
			loginAttemptsRemaining = MAX_LOGIN_ATTEMPTS;
		}

		previousAccountId = accountId;

		if (loginAttemptsRemaining == 0) {
			account.setRevoked(true);
			loginAttemptsRemaining = MAX_LOGIN_ATTEMPTS;
		}
	}

	public void deleteAccount(String accountId)
			throws UserAccountNotFoundException, UserAccountRevokedException, UserAccountSignedInException {
		account = UserAccount.find(accountId);

		if (account == null) {
			throw new UserAccountNotFoundException();
		}

		// We don't allow to delete revoked accounts
		if (account.isRevoked()) {
			throw new UserAccountRevokedException();
		}
		
		// We don't allow to delete signed in accounts
		if (account.isSignedIn()) {
			throw new UserAccountSignedInException();
		}

		UserAccount.delete(accountId);
	}
}
