package org.udg.caes.signin.state;

import mockit.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.udg.caes.signin.userAccount.UserAccount;
import org.udg.caes.signin.userLogin.*;


public class TestDelete {

    @Test
    public void testDeleteAccount() throws UserAccountNotFoundException, UserAccountRevokedException, UserAccountSignedInException {

        final String pw = "pw";
        final String usr = "usr";

        new MockUp<UserAccount>() {

            @Mock
            UserAccount find(String usr) {return new UserAccount(usr, pw);}

            @Mock
            boolean isSignedIn(){return false;}

            @Mock
            boolean isRevoked(){return false;}

            @Mock(invocations = 1)
            void delete(String accountId){assertEquals(usr, accountId);}
        };

        new SigninService().deleteAccount(usr);
    }

    @Test(expected = UserAccountSignedInException.class)
    public void testDeleteAccountIfIsSignedIn() throws UserAccountNotFoundException, UserAccountRevokedException, UserAccountSignedInException {

        final String pw = "pw";
        final String usr = "usr";

        new MockUp<UserAccount>() {

            @Mock
            UserAccount find(String usr) {return new UserAccount(usr, pw);}

            @Mock
            boolean isSignedIn(){return true;}

        };

        new SigninService().deleteAccount(usr);
    }

    @Test(expected = UserAccountRevokedException.class)
    public void testDeleteAccountIfIsRevoked() throws UserAccountNotFoundException, UserAccountRevokedException, UserAccountSignedInException {

        final String pw = "pw";
        final String usr = "usr";

        new MockUp<UserAccount>() {

            @Mock
            UserAccount find(String usr) {return new UserAccount(usr, pw);}

            @Mock
            boolean isRevoked(){return true;}

        };

        new SigninService().deleteAccount(usr);
    }

    @Test(expected = UserAccountNotFoundException.class)
    public void testDeleteAccountIfItNotFound() throws UserAccountNotFoundException, UserAccountRevokedException, UserAccountSignedInException {

        final String pw = "pw";
        final String usr = "usr";

        new MockUp<UserAccount>() {

            @Mock
            UserAccount find(String usr) {return null;}

        };

        new SigninService().deleteAccount(usr);
    }

}