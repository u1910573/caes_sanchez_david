package org.udg.caes.signin.behaviour;
import static org.junit.Assert.*;

import mockit.*;
import org.junit.*;
import org.udg.caes.signin.userAccount.UserAccount;
import org.udg.caes.signin.userLogin.*;

public class TestDelete {

    @Tested SigninService tested;


    @Test
    public void testDeleteAccount(@Mocked final UserAccount userMock) throws UserAccountNotFoundException, UserAccountRevokedException, UserAccountSignedInException {
        new NonStrictExpectations(){{
            UserAccount.find("pp"); result = userMock;
            userMock.isSignedIn(); result = false;
            userMock.isRevoked(); result = false;
        }};

        tested.deleteAccount("pp");



        new Verifications(){{
            userMock.delete("pp");
            times = 1;
        }};
    }

    @Test(expected = UserAccountSignedInException.class)
    public void testDeleteAccountIfIsSignedIn(@Mocked final UserAccount userMock) throws UserAccountNotFoundException, UserAccountRevokedException, UserAccountSignedInException {
        new NonStrictExpectations(){{
            UserAccount.find("pp"); result = userMock;
            userMock.isSignedIn(); result = true;
        }};

        tested.deleteAccount("pp");
    }

    @Test(expected = UserAccountRevokedException.class)
    public void testDeleteAccountIfIsRevoked(@Mocked final UserAccount userMock) throws UserAccountNotFoundException, UserAccountRevokedException, UserAccountSignedInException {
        new NonStrictExpectations(){{
            UserAccount.find("pp"); result = userMock;
            userMock.isRevoked(); result = true;
        }};

        tested.deleteAccount("pp");
    }

    @Test(expected = UserAccountNotFoundException.class)
    public void testDeleteAccountIfItNotFound(@Mocked final UserAccount userMock) throws UserAccountNotFoundException, UserAccountRevokedException, UserAccountSignedInException {
        new NonStrictExpectations(){{
            UserAccount.find("pp"); result = null;
        }};

        tested.deleteAccount("pp");
    }

}
