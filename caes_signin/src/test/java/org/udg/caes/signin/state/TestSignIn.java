package org.udg.caes.signin.state;

import mockit.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.udg.caes.signin.userAccount.UserAccount;
import org.udg.caes.signin.userLogin.AccountLoginLimitReachedException;
import org.udg.caes.signin.userLogin.SigninService;
import org.udg.caes.signin.userLogin.UserAccountNotFoundException;
import org.udg.caes.signin.userLogin.UserAccountRevokedException;


public class TestSignIn {

    @Test
    public void testSignInOk() throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException {

        final String pw = "pw";
        final String usr = "usr";

        new MockUp<UserAccount>(){

            @Mock
            UserAccount find(String usr1){return new UserAccount(usr,pw);}

            @Mock
            boolean passwordMatches(String candidatePassword){return true;}

            @Mock
            boolean isSignedIn(){return false;}

            @Mock
            boolean isRevoked(){return false;}

            @Mock(invocations = 1)
            void setSignedIn(boolean value){assertEquals(true, value);}

        };
        new SigninService().signin(usr,pw);
    }

    @Test(expected = AccountLoginLimitReachedException.class)
    public void testSignInIfIsSignIn() throws AccountLoginLimitReachedException, UserAccountNotFoundException, UserAccountRevokedException {

        final String pw = "pw";
        final String usr = "usr";

        new MockUp<UserAccount>(){

            @Mock
            UserAccount find(String usr1){return new UserAccount(usr,pw);}

            @Mock
            boolean passwordMatches(String candidatePassword){return true;}

            @Mock
            boolean isSignedIn(){return true;}
        };
        new SigninService().signin(usr,pw);
    }

    @Test(expected = UserAccountRevokedException.class)
    public void testSignInIfIsRevoked() throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException {

        final String pw = "pw";
        final String usr = "usr";

        new MockUp<UserAccount>(){

            @Mock
            UserAccount find(String usr1){return new UserAccount(usr,pw);}

            @Mock
            boolean passwordMatches(String candidatePassword){return true;}

            @Mock
            boolean isSignedIn(){return false;}

            @Mock
            boolean isRevoked(){return true;}
        };
        new SigninService().signin(usr,pw);
    }

    @Test(expected = UserAccountNotFoundException.class)
    public void testSignInIfAccountNotFound() throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException {

        final String pw = "pw";
        final String usr = "usr";

        new MockUp<UserAccount>(){

            @Mock
            UserAccount find(String usr1){return null;}
        };
        new SigninService().signin(usr,pw);
    }

    @Test
    public void testSignInIfNoPasswordMatchesOneTime() throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException {

        final String pw = "pw";
        final String usr = "usr";

        new MockUp<UserAccount>() {

            @Mock
            UserAccount find(String usr1) {return new UserAccount(usr, pw);}

            @Mock
            boolean passwordMatches(String candidatePassword) {return false;}

            @Mock(invocations = 0)
            void setSignedIn(boolean value){assertEquals(true, value);}
        };
        new SigninService().signin(usr,pw);
    }

    @Test
    public void testSignInIfNoPasswordMatchesThreeTimes() throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException {

        final String pw = "pw";
        final String usr = "usr";

        new MockUp<UserAccount>() {

            @Mock(invocations = 3)
            UserAccount find(String usr1) {return new UserAccount(usr, pw);}

            @Mock(invocations = 3)
            boolean passwordMatches(String candidatePassword) {return false;}

            @Mock(invocations = 1)
            void setRevoked(boolean value) {assertEquals(true, value);}
        };
        SigninService signIn = new SigninService();

        signIn.signin(usr,pw);
        signIn.signin(usr,pw);
        signIn.signin(usr,pw);
    }

    @Test
    public void testSignInAlternateTwoUsers() throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException {

        final String pw1 = "pw";
        final String usr1 = "usr";

        final String pw2 = "pw2";
        final String usr2 = "usr2";

        new MockUp<UserAccount>() {

            @Mock(invocations = 4)
            UserAccount find(String usr1) {return new UserAccount(usr1, pw1);}

            @Mock(invocations = 4)
            boolean passwordMatches(String candidatePassword) {return false;}

            @Mock(invocations = 0)
            void setRevoked(boolean value) {assertEquals(true, value);}
        };
        SigninService signIn = new SigninService();

        signIn.signin(usr1,pw1);
        signIn.signin(usr1,pw1);
        signIn.signin(usr2,pw2);
        signIn.signin(usr1,pw1);

    }

}
