package org.udg.caes.signin.behaviour;
import static org.junit.Assert.*;

import mockit.*;
import org.junit.*;
import org.udg.caes.signin.userAccount.UserAccount;
import org.udg.caes.signin.userLogin.*;

public class TestSignIn {

    @Tested SigninService tested;

    @Test
    public void testSignInOk(@Mocked final UserAccount userMock) throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException {
        new NonStrictExpectations(){{
            UserAccount.find("pp"); result = userMock; times=1;
            userMock.passwordMatches(anyString); result = true;
            userMock.isSignedIn(); result = false;
            userMock.isRevoked(); result = false;
        }};

        tested.signin("pp","p");

        new Verifications(){{
            userMock.setSignedIn(true);
            times = 1;
        }};
    }

    @Test(expected = AccountLoginLimitReachedException.class)
    public void testSignInIfIsSignIn(@Mocked final UserAccount userMock) throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException {
        new NonStrictExpectations(){{
            UserAccount.find("pp"); result = userMock; times=1;
            userMock.passwordMatches(anyString); result = true;
            userMock.isSignedIn(); result = true;
        }};

        tested.signin("pp","p");
    }

    @Test(expected = UserAccountRevokedException.class)
    public void testSignInIfIsRevoked(@Mocked final UserAccount userMock) throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException {
        new NonStrictExpectations(){{
            UserAccount.find("pp"); result = userMock; times=1;
            userMock.passwordMatches(anyString); result = true;
            userMock.isSignedIn(); result = false;
            userMock.isRevoked(); result = true;
        }};

        tested.signin("pp","p");
    }

    @Test(expected = UserAccountNotFoundException.class)
    public void testSignInIfAccountNotFound(@Mocked final UserAccount userMock) throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException {
        new NonStrictExpectations(){{
            UserAccount.find("pp"); result = null; times=1;
        }};

        tested.signin("pp","p");
    }

    @Test
    public void testSignInIfNoPasswordMatchesOneTime(@Mocked final UserAccount userMock) throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException {
        new NonStrictExpectations(){{
            UserAccount.find("pp"); result = userMock; times=1;
            userMock.passwordMatches(anyString); result = false; times = 1;

        }};

        tested.signin("pp","p");

        new Verifications(){{
            userMock.setSignedIn(true);
            times = 0;
        }};

    }

    @Test
    public void testSignInIfNoPasswordMatchesThreeTimes(@Mocked final UserAccount userMock) throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException {
        new NonStrictExpectations(){{
            UserAccount.find("pp"); result = userMock; times=3;
            userMock.passwordMatches(anyString); result = false; times = 3;
        }};

        tested.signin("pp","p");
        tested.signin("pp","p");
        tested.signin("pp","p");


        new Verifications(){{
            userMock.setRevoked(true);
            times = 1;
        }};
    }

    @Test
    public void testSignInAlternateTwoUsers(@Mocked final UserAccount userMock, @Mocked final UserAccount userMock2) throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException {
        new NonStrictExpectations(){{
            UserAccount.find("pp"); result = userMock; times=3;
            userMock.passwordMatches(anyString); result = false; times = 3;
            UserAccount.find("qq"); result = userMock2; times=1;
            userMock2.passwordMatches(anyString); result = true; times = 1;

        }};

        tested.signin("pp","p");
        tested.signin("pp","p");
        tested.signin("qq","q");
        tested.signin("pp","p");

        new Verifications(){{
            userMock2.setSignedIn(true);
            times = 1;
            userMock.setRevoked(true);
            times = 0;
        }};
    }

}
